# Let it snow � GNOME Shell Extension
**Bring winter to your desktop: unicode snowflakes falling on your screens.**

Available at [GNOME Shell Extensions](https://extensions.gnome.org/extension/1547/let-it-snow/).

![screenshot](screenshot.png)

### Features:
- snow doesn't fall over currently active window
- use slider to choose how much snow you want
- works with multiple monitors

Inspired by [gsnow](https://extensions.gnome.org/extension/1156/gsnow/).